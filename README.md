# dupli_finder

## Python script for finding duplicate files.

This scriptlet compares all files of a folder (and their subfoldes),
by their exact byte sizes. If two or more files with the same identical 
filesize have been found, also their SHA256 hashes ("unique fingerprints")
are compared for a even better distinction. For perfomance reasons, files
bigger than 50 MB aren't hashed. Two or more files bigger than 50 MB -
if exactly of the same number / amounts of bytes - are most likely being
identical anyway and it is up to the user to investigate if they are infact
the same and thus will be listed nevertheless.

## Usage of this script

```python3 dupli_finder.py <path-to-directory> <subfolders-yes-or-no> <path-to-save-results.csv>```

You may call the script with those three additional sys-arguments, the first one for the path to the directory of interest, the second as a yes or no ("y/n") flag, if all subfolders of that directory shall be considered recursively or not and a final third for the directory, to where the results.csv shall be written.

If no or too few sys-arguments are given or invalid arguments are supplied the user is prompted either for a valid directory of interest and/or if the subfolders shall be considered as well. The user won't be prompted for the results directory if not priveded with all arguments via the sys-args. The results directory needs to exist, The total disk usage of all files that exist more than once, will be printed out (and saved) as well.

If a dot "." is provided, the directory of interest will be the _current working directory_, meaning the directory from where the script is being called.

### Examples

```python3 dupli_finder.py /home/eckhard/Downloads```

```python3 dupli_finder.py ~/Pictures y```

```python3 dupli_finder.py . n```

```python3 dupli_finder.py```

```python3 dupli_finder.py /home/eckhard/Documents y ~/Desktop/dupli_results/ ```

## Notes
All files within .git/ folders are always ignored, as most of them are redundant by the nature of git.

This python3 script is WSL2.0 compatible.

## The logic behind
Earlier versions of the _dupli finder_ were hashing every single file they came accross and were comparing their SHA-1 hashes. This was rather expenisve for lots and lots of files, especially for large "non ordinary user files" (like pictures, music, documents, textfiles, etc. that usally fall into the 1 kB up to 50 MB range). To mitigate this drain of performance a switch has been introduced to compare files bigger than 50 MB only by their sizes in bytes, as the more bytes a file has, the more likely it is if a file of exactly the same size (down to the single byte) is found again, that they are infact identical. So for files in the range of hundreds of Megabytes up to Gigabytes (like Videos or disk images) of exactly of the same size, it's up to the user to differientate between those big duplicates that have been found.

In this version, the identical bytes-filesizes are the main necessary criteria for which and only then the hashes are calculated to differentiate those files even further. As the filesizes don't need to be calculated or measured - the filesystem will gladly tell which file uses what amount of disk space - the perfomance gain is of several orders of magnitudes. For this now lightning fast initial analysis the _dupli finder_ has transitioned from SHA-1 to SHA-256 for comparing those "few" of the same size, for even smaller (although already incredibly small) chances of collisions.

It is safe to assume that for the few ordinary user files (in that small size range) there aren't practically any collisions to be feared with SHA-256 and the now necessary criteria to have a higher than zero possibility that those files are identical, are infact that those files have to be of the same size.

In other words: two files are only then highly likely to be identical, and thus hash to the same value, if the numbers / amounts of their bytes are identical.
So for extra "safety", as in avoiding false duplicates (collisions) as best as possible, all files that are at least found twice with exactly the same size are also compared by their SHA-256 hashes.


## TODOs and improvement plans:

In future versions the user:
 - may set their personal filesize limit from the default of 50 Megabytes up to which files will be also compared by their hashes

 Other improvement plans:
  - better "results directory" handling
  - on KeyboardInterrupt show on which file the script was stopped and return results if any were found up to this point
  - Add a German translation for this README
  - prompt the user to ignore zero byte-files upon printout / before saving to results.txt
