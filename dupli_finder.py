""" This scriptlet compares all files of a folder (and their subfolders),
by their exact byte sizes. If two or more files with the same identical
filesize have been found, also their SHA256 hashes ("unique fingerprints")
are compared for a even better distinction. For performance reasons, files
bigger than 50 MB aren't hashed. Two or more files bigger than 50 MB -
if exactly of the same number / amounts of bytes - are most likely being
identical anyway and it is up to the user to investigate if they are in fact
the same and thus will be listed nevertheless. """

from datetime import datetime
import xxhash
import os
import sys

HASH_FILESIZE_LIMIT = 4e9

def calculate_file_hash(file_to_hash):
    """ Takes a file and returns the xxHash (xxh3) digest """
    hash_value = xxhash.xxh3_64()

    with open(file_to_hash, "rb") as current_file:

        for byte_block in iter(lambda: current_file.read(4096), b""):
            hash_value.update(byte_block)

    return hash_value.hexdigest()


def pretty_print_duplicates():
    """ Pretty Prints name and location of files from DUPLICATES """

    if found_duplicates:

        last_identifier = ''

        for current_identifier, fileobjects in found_duplicates.items():
            for file_object in fileobjects:

                if last_identifier != current_identifier:
                    last_identifier = current_identifier
                    print("\n" + '\033[1m' + str(last_identifier) + '\033[0m')

                #print(current_identifier, file_object.filepath)
                print(file_object.filepath)

        print("\nPossible waste of diskspace:", calc_disk_use())

    else:
        print("\nNo duplicates files found.\n")


def calc_disk_use():
    """ calculates the order of magnitude of DISKUSAGE and
        returns kB, MB, or GB für kilo-, Mega- or Gigabytes """

    diskusage = ''

    if DISK_USAGE < 1e3:
        diskusage = str(DISK_USAGE) + ' bytes\n'

    elif DISK_USAGE < 1e6:
        diskusage = str(round(DISK_USAGE/1e3, 2)) + " kB\n"

    elif DISK_USAGE < 1e9:
        diskusage = str(round(DISK_USAGE/1e6, 2)) + " MB\n"

    elif DISK_USAGE < 1e12:
        diskusage = str(round(DISK_USAGE/1e9, 2)) + " GB\n"

    elif DISK_WASTE < 1e15:
        diskusage = str(round(DISK_USAGE/1e12, 2)) + " TB\n"

    else:
        diskusage = 'OVER 9000!'

    return diskusage

################################################################################


def prompt_for_root_dir():
    """ user is being prompted for the root directory """
    while True:

        root_dir = input("Enter directory to check for duplicates: ")

        if os.path.isdir(root_dir):
            return os.path.abspath(root_dir)

        print("Directory can't be found.")


def prompt_for_subfolders():
    """ user is being prompted if files from all subfolders shall be considered """
    while True:

        consider_subfolders = input(
            "Shall files of all subfolders be included? (y/n): ")

        if consider_subfolders in 'yY':
            return True

        if consider_subfolders in 'nN':
            return False


def get_directory_from_user():
    """ Checks if second sys argument is a valid path, else prompts for input """

    if len(sys.argv) >= 2:

        if os.path.isdir(sys.argv[1]):
            return os.path.abspath(sys.argv[1])

        if sys.argv[1] == ".":
            return os.getcwd()

    return prompt_for_root_dir()


def get_decision_on_subfolders_from_user():
    """ Checks if third sys argument is yes or no, if subfolders shall
        be included into the search else prompts for input """

    if len(sys.argv) >= 3:

        if len(sys.argv[2]) == 1:

            if sys.argv[2].lower() == 'y':
                return True

            if sys.argv[2].lower() == 'n':
                return False

    return prompt_for_subfolders()

################################################################################


def save_to_file():
    """ if a valid path is provided as a third sys.argv and duplicates have been
        found, the results are being written to that paths results.csv """

    if found_duplicates:

        if len(sys.argv) >= 4 and os.path.isdir(sys.argv[3]):

            last_identifier = ''

            write_time = datetime.now().strftime("%Y%m%d_%H%M%S")
            results_file_name = '/results_' + write_time + '.csv'

            with open(sys.argv[3] + results_file_name, 'w') as results:

                results.write(
                    "Identifier;Filepath;Directory;Filename;Filesize\n")

                for current_identifier, fileobjects in found_duplicates.items():
                    for file_object in fileobjects:

                        if last_identifier != current_identifier:
                            last_identifier = current_identifier
                            results.write("\n")

                        line = file_object.print_properties()
                        results.write(line)

                results.write(
                    "\nPossible waste of diskspace: " + calc_disk_use())

################################################################################


class File:
    """ Collection of file properties """

    def __init__(self, filepath: str, filedir: str, filename: str, filesize: int):
        self.identifier = ''
        self.filepath = filepath
        self.filedir = filedir
        self.filename = filename
        self.filesize = filesize

    def set_identifier(self):
        """ determine this files identifier: hash or size """

        if self.filesize > HASH_FILESIZE_LIMIT:
            self.identifier = self.filesize
        else:
            self.identifier = calculate_file_hash(self.filepath)

    def print_properties(self):
        """ returns a semicolon separated file properties string """

        return str(self.identifier) + ";" + self.filepath + ";"\
            + self.filedir + ";" + self.filename + \
            ";" + str(self.filesize) + "\n"

################################################################################


directory_of_interest = get_directory_from_user()
SCAN_SUBFOLDERS = get_decision_on_subfolders_from_user()

list_of_fileobjects = list()
list_of_unique_file_sizes = list()

found_duplicates = dict()
unique_identifiers = dict()

DISK_USAGE = 0

print("Dupli-Finder started. Searching for duplicates in:",
      directory_of_interest, "...")

for root, dirs, files in os.walk(directory_of_interest):
    for name in files:

        # get file path and size
        file_path = os.path.join(root, name)

        if not os.path.isfile(file_path):
            continue

        # ignore hidden folders
        if '/.' in file_path:
            continue

        file_size = os.path.getsize(file_path)

        # create File Object and add to list of fileobjects
        FILE = File(file_path, root, name, file_size)
        list_of_fileobjects.append(FILE)

        # memorize the filesizes
        if file_size not in list_of_unique_file_sizes:
            list_of_unique_file_sizes.append(file_size)

        # if this filesize has been seen before, go through
        # all file objects of the same size without an identifier
        # and set their identifying property.
        # add those to the list of fileobjects with identifiers.
        else:
            for fileobject in list_of_fileobjects:
                if fileobject.filesize == file_size and fileobject.identifier == '':
                    fileobject.set_identifier()

    # don't descend into subfolders if the user does not want to look there
    if not SCAN_SUBFOLDERS:
        break

################################################################################

# now we have a list of all objects, some of them have identifiers (size or hash)
# let's got through those with identifiers and keep track of those seen before

for fileobject in list_of_fileobjects:

    if fileobject.identifier == '':
        continue

    # first time this identifier is being found, so lets add it to the
    # unique identifiers dictionary under its identifier-key
    if fileobject.identifier not in unique_identifiers:
        unique_identifiers[fileobject.identifier] = fileobject
        continue

    # if our object made it this far, it means the following:
    # it has an identifier, and this identifier has been found before.
    # so lets add the first fileobject with this identifier to our
    # duplicates dictionary and also add the current fileobject as well,
    # because we have found it before
    if fileobject.identifier not in found_duplicates:
        found_duplicates[fileobject.identifier] = [
            unique_identifiers[fileobject.identifier], ]

    found_duplicates[fileobject.identifier].append(fileobject)

    # at last, add the filesize of this file once to the total disk usage
    # because at this point we know it is in fact a duplicate in some way.
    # As "same filesize" is the main criteria, we know this is redundant disk usage
    # under the assumption large files of the same bytes are highly likely to be the same
    DISK_USAGE += fileobject.filesize

################################################################################
DISK_WASTE = calc_disk_use()

pretty_print_duplicates()
save_to_file()
